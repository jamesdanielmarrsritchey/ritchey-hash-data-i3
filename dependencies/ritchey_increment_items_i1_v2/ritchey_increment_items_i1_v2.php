<?php
#Name:Ritchey Increment Items i1 v2
#Description:Increment items by 1 valid increment using content generation techniques. Returns items as an array on success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values. Unsupported items will trigger failure. '' (an empty value) cannot be used in array, except for an empty array. This means "''" is valid, but "'', $value2, $value3" is not.
#Arguments:'array' is an array containing supported items to increment. 'item_list' is an array of supported items to increment by. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):array:array:required,item_list:array:required,display_errors:bool:optional
#Content:
if (function_exists('ritchey_increment_items_i1_v2') === FALSE){
function ritchey_increment_items_i1_v2($array, $item_list, $display_errors = NULL){
	$errors = array();
	if (@empty($array) === TRUE){
		$errors[] = "array";
	} else if (@is_array($array) === FALSE){
		$errors[] = "array";
	}
	if (@empty($item_list) === TRUE){
		$errors[] = "item_list";
	} else if (@is_array($item_list) === FALSE){
		$errors[] = "item_list";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	} else if ($display_errors === TRUE){
		#Do Nothing
	} else if ($display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task []
	if (@empty($errors) === TRUE){
		###Reverse array
		$array = @array_reverse($array);
		###Increment the first item. When it exceeds item_list it loops back to item_list[0], and the next value is incremented by 1. If a next doesn't exist create it as item_list[0].
		$skip_check_if_entire_array_same = FALSE;
		foreach ($array as &$value) {
			if ($value === ''){
					if (count($array) > 1){
						$errors[] = "item_list (contains '' as value at key above [0].)";
						goto result;
					}
					$value = $item_list[0];
					$skip_check_if_entire_array_same = TRUE;
					goto break_increment;
			} else if (isset($value) === TRUE) {
				$check = FALSE;
				for ($i = 0; $i < count($item_list); $i++) {
					$ii = $i + 1;
					if ($value === $item_list[$i]) {
						if ($ii < count($item_list)){
							$value = $item_list[$ii];
							goto break_increment;
						} else {
							$value = $item_list[0];
						}
						$check = TRUE;
					}
				}
				if ($check === FALSE){
					$errors[] = "item_list (unsupported item:\"{$value}\")";
					goto result;
				}
			}
		}
		unset($value);
		break_increment:
		###Unreverse array
		$array = @array_reverse($array);
		###Check if entire array is $item_list[0]'s, and if it is append $item_list[0] to the end to increment to next range.
		if ($skip_check_if_entire_array_same === FALSE){
			$check = FALSE;
			foreach ($array as &$value) {
				if ($value !== $item_list[0]){
					$check = TRUE;
				}
			}
			unset($value);
			if ($check === FALSE){
				$array[] = $item_list[0];
			}
		}
	}
	result:
	##Display Errors
	if ($display_errors === TRUE){
		if (@empty($errors) === FALSE){
			$message = @implode(", ", $errors);
			if (function_exists('ritchey_increment_items_i1_v2_format_error') === FALSE){
				function ritchey_increment_items_i1_v2_format_error($errno, $errstr){
					echo $errstr;
				}
			}
			set_error_handler("ritchey_increment_items_i1_v2_format_error");
			trigger_error($message, E_USER_ERROR);
		}
	}
	##Return
	if (@empty($errors) === TRUE){
		return $array;
	} else {
		return FALSE;
	}
}
}
?>