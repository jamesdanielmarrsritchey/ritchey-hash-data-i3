<?php
#Name:Ritchey Hash Data i3 v1
#Description:Process data with Ritchey Hashing Algorithm v3. Returns checksum as a string success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values. This process is extremely intensive, and the workload increases with data size so it is only practical for very small data sets.
#Arguments:'string' is a string containing the data to hash. 'max_length' (optional) is a number specifying the maximum length to allow the checksum to be. Internally all this does is rehash the checksum until a small enough one is reached. 'include_metadata' (optional) is a boolean indicating whether to include meta data along with the checksum in the return. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):string:string:required,max_length:integer:optional,include_metadata:bool:optional,display_errors:bool:optional
#Content:
if (function_exists('ritchey_hash_data_i3_v1') === FALSE){
function ritchey_hash_data_i3_v1($string, $max_length = NULL, $include_metadata = NULL, $display_errors = NULL){
	$errors = array();
	$progress = '';
	if (@isset($string) === FALSE){
		$errors[] = "string";
	}
	if ($max_length === NULL){
		$max_length = FALSE;
	} else if ($max_length === FALSE){
		#Do nothing
	} else if (@is_int($max_length) === TRUE){
		#Do Nothing
	} else {
		$errors[] = "max_length";
	}
	if ($include_metadata === NULL){
		$include_metadata = FALSE;
	} else if ($include_metadata === TRUE){
		#Do Nothing
	} else if ($include_metadata === FALSE){
		#Do Nothing
	} else {
		$errors[] = "include_metadata";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	} else if ($display_errors === TRUE){
		#Do Nothing
	} else if ($display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task [Convert each byte to a decimal representation. Increment checksum by that number. Increment checksum by the previous number. Increment checksum text by the offset number of the current byte. Increment checksum by the current checksum increments. If checksum exceeds max_length reprocess the checksum until it doesn't. If include_metadata is set, add variable information to checksum. Return the checksum.]
	if (@empty($errors) === TRUE){
		$checksum = array('');
		$checksum_item_list = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '!', '?', '@', '#', '$', '&', '-', '+', '=');
		###Break into an array of bytes, and process them individually
		$string = @str_split($string, 1);
		$loops = TRUE;
		while ($loops === TRUE){
		$i = 0;
		$checksum_i = 0;
		$previous_value_decimal = FALSE;
		foreach ($string as &$value){
			####Convert byte to decimal representation [This will be an array of integers, convert to an integer.]
			$location = realpath(dirname(__FILE__));
			require_once $location . '/dependencies/ritchey_data_to_decimal_representation_i1_v2/ritchey_data_to_decimal_representation_i1_v2.php';
			$value_decimal = ritchey_data_to_decimal_representation_i1_v2($value, FALSE);
			if (@count($value_decimal) !== 1){
				$errors[] = "value_decimal";
				goto result;
			} else {
				$value_decimal = $value_decimal[0];
			}
			$saved_value_decimal = $value_decimal;
			####Increment checksum by value_decimal
			$location = realpath(dirname(__FILE__));
			require_once $location . '/dependencies/ritchey_increment_items_i1_v2/ritchey_increment_items_i1_v2.php';
			for ($value_decimal_i = 0; $value_decimal_i < $value_decimal; $value_decimal_i++) {
				if ($checksum === FALSE){
					$errors[] = "checksum";
					goto result;
				}
				$checksum = ritchey_increment_items_i1_v2($checksum, $checksum_item_list, FALSE);
				$checksum_i++;
			}
			####Increment checksum by previous_value_decimal
			if ($previous_value_decimal !== FALSE){
				$location = realpath(dirname(__FILE__));
				require_once $location . '/dependencies/ritchey_increment_items_i1_v2/ritchey_increment_items_i1_v2.php';
				for ($previous_value_decimal_i = 0; $previous_value_decimal_i < $previous_value_decimal; $previous_value_decimal_i++) {
					if ($checksum === FALSE){
						$errors[] = "checksum";
						goto result;
					}
					$checksum = ritchey_increment_items_i1_v2($checksum, $checksum_item_list, FALSE);
					$checksum_i++;
				}
			}
			####Increment checksum by offset of $value (eg: $i)
			if ($i > 0){
				$location = realpath(dirname(__FILE__));
				require_once $location . '/dependencies/ritchey_increment_items_i1_v2/ritchey_increment_items_i1_v2.php';
				for ($offset_i = 0; $offset_i < $i; $offset_i++) {
					if ($checksum === FALSE){
						$errors[] = "checksum";
						goto result;
					}
					$checksum = ritchey_increment_items_i1_v2($checksum, $checksum_item_list, FALSE);
					$checksum_i++;
				}
			}
			####Increment checksum by current number of checksum increments
			if ($checksum_i > 0){
				$location = realpath(dirname(__FILE__));
				require_once $location . '/dependencies/ritchey_increment_items_i1_v2/ritchey_increment_items_i1_v2.php';
				$checksum_i_old = $checksum_i;
				for ($checksum_ii = 0; $checksum_ii < $checksum_i_old; $checksum_ii++) {
					if ($checksum === FALSE){
						$errors[] = "checksum";
						goto result;
					}
					$checksum = ritchey_increment_items_i1_v2($checksum, $checksum_item_list, FALSE);
					$checksum_i++;
				}
			}
			$i++;
			$previous_value_decimal = $saved_value_decimal;
		}
		unset($value);
		if (@is_int($max_length) === TRUE){
			if (@strlen(implode($checksum)) > $max_length){
				$loops = TRUE;
				$string = $checksum;
				$checksum = array('');
			} else {
				$loops = FALSE;
			}
		} else {
			$loops = FALSE;
		}
		}
		$checksum = @implode($checksum);
		###If include_metadata is TRUE, add to return
		if ($include_metadata === TRUE){
			if ($max_length === FALSE){
				$checksum = "RHA3;checksum:{$checksum}";
			} else {
				$checksum = "RHA3;max_length:{$max_length};checksum:{$checksum}";
			}
		}
	}
	result:
	##Display Errors
	if ($display_errors === TRUE){
		if (@empty($errors) === FALSE){
			$message = @implode(", ", $errors);
			if (function_exists('ritchey_hash_data_i3_v1_format_error') === FALSE){
				function ritchey_hash_data_i3_v1_format_error($errno, $errstr){
					echo $errstr;
				}
			}
			set_error_handler("ritchey_hash_data_i3_v1_format_error");
			trigger_error($message, E_USER_ERROR);
		}
	}
	##Return
	if (@empty($errors) === TRUE){
		return $checksum;
	} else {
		return FALSE;
	}
}
}
?>